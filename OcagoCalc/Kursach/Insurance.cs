﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursach
{

    class CostInfo
    {
        public double Cost { get; set; }
    }
    interface IObservable
    {
        void AddObserver(IObserver o);
        void RemoveObserver(IObserver o);
        void NotifyObservers();
    }
    class Insurance : IObservable
    {
        CostInfo cInfo = new CostInfo(); // информация о цене

        List<IObserver> observers;
        public Insurance()
        {
            observers = new List<IObserver>(); // Список наблюдателей
        }
        public void AddObserver(IObserver o) // Добавление наблюдателя
        {
            observers.Add(o);
        }
        public void RemoveObserver(IObserver o)// Удаление наблюдателя
        {
            observers.Remove(o);
        }
        public void NotifyObservers() // Оповещение наблюдателя
        {
            foreach (IObserver o in observers)
            {
                o.Update(cInfo);
            }
        }
        public void FinishCost(double tb, double kt, double kvs, double ko, double km, double ks, double kn) // Имитация вычисления стоимости по коэффициенту
        {
            cInfo.Cost = tb * kt * kvs * ko * km * ks * kn;
            NotifyObservers();
        }
    }
}
