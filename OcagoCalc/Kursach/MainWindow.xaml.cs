﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kursach
{
    interface IOther // Создание интерфейса для паттерна
    {
        void Other(); // Создание метода
    }
    
    public partial class CalcOsago : Window
    {

        double tb = 0, kt = 0, kvs = 0, ko = 0, km = 0, ks = 0, kn = 0;
        public void Tbd()
        {
            int ur = 2573, fiz = 3432;
            // Базовый тариф
            if (tippol.SelectedIndex == 0)
                tb = ur;
            if (tippol.SelectedIndex == 1)
                tb = fiz;
            if (diagKar.SelectedIndex == 1)
                tb += 1000;
        }
        public void Kmd()
        {
            // Мощность

            if (Convert.ToInt32(mosh.Text) <= 50)
                km = 0.6;

            if (Convert.ToInt32(mosh.Text) > 50 & (Convert.ToInt32(mosh.Text) <= 70))
                km = 1;

            if (Convert.ToInt32(mosh.Text) > 70 & (Convert.ToInt32(mosh.Text) <= 100))
                km = 1.1;

            if (Convert.ToInt32(mosh.Text) > 100 & (Convert.ToInt32(mosh.Text) <= 120))
                km = 1.2;

            if (Convert.ToInt32(mosh.Text) > 120 & (Convert.ToInt32(mosh.Text) <= 150))
                km = 1.4;

            if (Convert.ToInt32(mosh.Text) > 150)
                km = 1.6;


        }

        public void Ktd()
        {
            // Коэффициент территории
            string[] a = { "Ессентуки", "Георгиевск", "Минеральные Воды" };
            if (regproz.Text == "Ставрополь" || regproz.Text == "Михайловск")
                kt = 1.2;
            else if (a.Any(x => x == regproz.Text))
                kt = 1;
            else
                kt = 0.7;
        }

        public void Kod()
        {
            // тип страховки

            if (tipstr.SelectedIndex == 0)

                ko = 1;

            if (tipstr.SelectedIndex == 1)
            {
                ko = 1.8;
                vis.IsEnabled = true;
            }
        }

        public void Kvsd()
        {
            // возраст и стаж

            if (vis.IsEnabled == false)
                kvs = 1;
            else
            {
                if (vis.SelectedIndex == 0)

                    kvs = 1.8;
                if (vis.SelectedIndex == 1)

                    kvs = 1.7;
                if (vis.SelectedIndex == 2)

                    kvs = 1.6;
                if (vis.SelectedIndex == 3)

                    kvs = 1;

            }
        }

        private void Narush_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (narush.SelectedIndex == 0 || narush.SelectedIndex == 1)
                vychis.IsEnabled = true;
        }

        private void Mosh_KeyUp(object sender, KeyEventArgs e)
        {

            //char s = (char)60-69;
            if (e.Key < Key.D0 || e.Key > Key.D9)
                e.Handled = true;
        }

        private void Tipstr_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (mosh.Text != "" & regproz.Text != "" & tipstr.SelectedIndex != -1)
                button.IsEnabled = true;
        }

        private void DiagKar_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tippol.SelectedIndex == -1 & diagKar.SelectedIndex == -1)
                button.IsEnabled = false;
            else button.IsEnabled = true;
        }

        private void Regproz_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key < Key.D0 || e.Key > Key.D9)
                e.Handled = false;
            else e.Handled = true;
        }

        public void Ksd()
        {
            // период использования id5

            if (perIsp.Value == 3)

                ks = 0.5;
            if (perIsp.Value == 4)

                ks = 0.6;
            if (perIsp.Value == 5)

                ks = 0.65;
            if (perIsp.Value == 6)

                ks = 0.7;
            if (perIsp.Value == 7)

                ks = 0.8;
            if (perIsp.Value == 8)

                ks = 0.9;
            if (perIsp.Value == 9)

                ks = 0.95;
            if (perIsp.Value == 10 || perIsp.Value == 11 || perIsp.Value == 12)

                ks = 1;
        }

        public void Knd()
        {
            // нарушения, ДТП id6

            if (narush.SelectedIndex == 0)

                kn = 1.5;
            else kn = 1;
        }

        bool isEnded = false;
       
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            diagKar.IsEnabled = false;
            if (isEnded == false)
                if (stack1.Visibility == Visibility.Hidden)
                {
                    stack1.Visibility = Visibility.Visible;
                    button.IsEnabled = false;

                }
                else if (stack2.Visibility == Visibility.Hidden)
                {
                    if (tipstr.SelectedIndex == 1)
                        vis.IsEnabled = false;
                    stack1.Visibility = Visibility.Hidden;
                    stack2.Visibility = Visibility.Visible;
                    //isEnded = true;
                }
                else if (stack3.Visibility == Visibility.Hidden)
                {
                    stack2.Visibility = Visibility.Hidden;
                    stack3.Visibility = Visibility.Visible;

                }
            if (stack2.Visibility == Visibility.Visible)
                button.IsEnabled = false;
        }
        private void Process_Click(object sender, RoutedEventArgs e)
        {
            Send send = new Send();

            stack2.Visibility = Visibility.Hidden;
            stack3.Visibility = Visibility.Visible;
            Tbd();
            Ktd();
            Kvsd();
            Kod();
            Kmd();
            Ksd();
            Knd();
            double formula = tb * kt * kvs * ko * km * ks * kn;

            itog.Text = Convert.ToString(formula);
            zan.Visibility = Visibility.Visible;
            button.Visibility = Visibility.Hidden;

            itog.IsEnabled = false;

            send.SendToEmail("nameEmail@mail.ru", "Иванов");
        }


        public void ReturnOSAGO_Click(object sender, RoutedEventArgs e)
        {
            diagKar.IsEnabled = true;
            stack1.Visibility = Visibility.Hidden;
            stack2.Visibility = Visibility.Hidden;
            stack3.Visibility = Visibility.Hidden;
            tippol.SelectedIndex = -1;
            diagKar.SelectedIndex = -1;
            mosh.Text = "";
            regproz.Text = "";
            tipstr.SelectedIndex = -1;
            vis.SelectedIndex = -1;
            perIsp.Value = 0;
            narush.SelectedIndex = -1;
            vychis.IsEnabled = false;
            zan.Visibility = Visibility.Hidden;
            button.Visibility = Visibility.Visible;

        }


        public CalcOsago()
        {
            InitializeComponent();
            Car auto = new Car(4, "Toyota", new BigTrailer()); // Заполнение данными и вызов метода С БОЛЬШИМ ПРИЦЕПОМ
            auto.Other(); // Вызов метода именно нужного нам
            button.IsEnabled = false;
            Singleton s1 = Singleton.GetInstance();
            if (s1 == null)
            {
                MessageBox.Show("Заполнены не все данные!");
            }
            else
            {
                //MessageBox.Show("Добро пожаловать!");
            }

            Insurance insur = new Insurance();
            Client cln = new Client("Иван Васильевич", insur);
            // имитация оформления полиса
            insur.FinishCost(1,1.3,1.2,1.1,1,1.4,0.7);// Выглядит убого, но это не конечный вид, в готовом будет выглядеть по другому (не это не точно)
            // клиент перестает наблюдать за изменением коэффициентов
            cln.DeleteObs();

        }
    }
}