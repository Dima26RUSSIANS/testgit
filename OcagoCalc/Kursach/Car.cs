﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursach
{
    class Car // Для примера делаю через класс Кар
    {
        protected int pass; // кол-во пассажиров
        protected string model; // модель автомобиля
        public Car(int count, string model, IOther oth)
        {
            pass = count;
            this.model = model;
            other = oth;
        }
        public IOther other { get; set; }
        public void Other()
        {
            other.Other();
        }
    }
}
