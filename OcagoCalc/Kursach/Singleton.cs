﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursach
{
    class Singleton
    {
        // Конструктор Одиночки 
        private Singleton() { }
        // Объект одиночки 
        private static Singleton _instance;
        // Это статический метод, управляющий доступом к экземпляру одиночки.
        public static Singleton GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Singleton();
            }
            return _instance;
        }
    }
}
