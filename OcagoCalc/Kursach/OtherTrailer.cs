﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Kursach
{
    class OtherTrailer : IOther
    {
        public void Other()// Метод будет вызываться, если у машины будет другой прицеп
        {
            MessageBox.Show("Используется другой прицеп!");
        }
    }
}
