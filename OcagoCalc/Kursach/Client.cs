﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kursach
{
    interface IObserver
    {
        void Update(Object ob);
    }
    class Client : IObserver
    {
        public string ClientsName { get; set; }
        IObservable cost;
        public Client(string name, IObservable obs)
        {
            this.ClientsName = name;
            cost = obs;
            cost.AddObserver(this);
        }
        public void Update(object ob)
        {
            CostInfo cInfo = (CostInfo)ob;
            Console.WriteLine("Для Клиента: {0} изменилась цена;  Цена страховки: {1}", this.ClientsName, cInfo.Cost);
        }
        public void DeleteObs()
        {
            cost.RemoveObserver(this);
            cost = null;
        }
    }
}
